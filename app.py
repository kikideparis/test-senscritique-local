# import main Flask class and request object
from flask import Flask, request

# create the Flask app
app = Flask(__name__)

@app.route('/titre-exemple')
def titre_exemple():
    # if key doesn't exist, returns None
    note = request.args.get('note')

    # if key doesn't exist, returns a 400, bad request error
    genre = request.args['genre']

    # if key doesn't exist, returns None
    titre = request.args.get('titre')

    return '''
              <h1> La valeur de la note est : {}</h1>
              <h1> Le genre de ce film est : {}</h1>
              <h1> Le titre du film est : {}'''.format(note, genre, titre)

if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(debug=True, port=5000)